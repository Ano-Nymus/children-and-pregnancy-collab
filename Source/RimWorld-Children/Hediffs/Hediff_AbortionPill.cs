﻿namespace RimWorldChildren
{
    using RimWorld;
    using Verse;

    public class Hediff_AbortionPill : HediffWithComps
    {
        private int hediff_age = 0;

        public override void Tick ()
        {
            // Drug takes time to take effect
            BodyPartRecord torso = pawn.RaceProps.body.AllParts.Find (x => x.def == BodyPartDefOf.Torso);
            if (hediff_age == 2500) {
                foreach (string hediffDefName in Hediff_Pregnancy.HediffOfClass.Values) {
                    if (pawn.health.hediffSet.HasHediff(HediffDef.Named(hediffDefName), torso) && HediffDef.Named(hediffDefName).HasComp(typeof(CompAbortable))) {
                        Hediff preggo = pawn.health.hediffSet.GetFirstHediffOfDef(HediffDef.Named(hediffDefName));
                        CompAbortable abortable = preggo.TryGetComp<CompAbortable>();
                        if (preggo.Severity < abortable.Props.maxSeverity) {
                            abortable.Abort(null);

                        }
                    }
                }
            }

            if (hediff_age > 16000) {
                if (Rand.Range(1, 1000) == 1) {
                    // Bleeding from taking the pill
                    pawn.health.DropBloodFilth ();
                }
            }
            hediff_age += 1;
        }

        public override bool Visible {
            get { return true; }
        }
    }
}