﻿namespace RimWorldChildren {
    using System.Collections.Generic;
    using RimWorld;
    using Verse;

    /// <summary>
    /// Defines a pregnancy hediff that can result in miscarriage given certain circumstances.
    /// </summary>
    public class CompMiscarriable : HediffComp {

        protected const int MiscarryCheckInterval = 1000;
        protected const float MTBMiscarryStarvingDays = 0.5f;
        protected const float MTBMiscarryWoundedDays = 0.5f;

        public CompProperties_Miscarriable Props => (CompProperties_Miscarriable)props;

        /// <summary>
        /// Determine if the pawn is significantly wounded to the deteriment of the child
        /// </summary>
        protected virtual bool IsSeverelyWounded {
            get {
                float num = 0;
                List<Hediff> hediffs = Pawn.health.hediffSet.hediffs;
                for (int i = 0; i < hediffs.Count; i++) {
                    if (hediffs[i] is Hediff_Injury && !hediffs[i].IsPermanent()) {
                        num += hediffs[i].Severity;
                    }
                }
                List<Hediff_MissingPart> missingPartsCommonAncestors = Pawn.health.hediffSet.GetMissingPartsCommonAncestors();
                for (int j = 0; j < missingPartsCommonAncestors.Count; j++) {
                    if (missingPartsCommonAncestors[j].IsFresh) {
                        num += missingPartsCommonAncestors[j].Part.def.GetMaxHealth(Pawn);
                    }
                }
                return num > 38 * Pawn.RaceProps.baseHealthScale;
            }
        }

        protected virtual void Miscarry(Pawn baby, string messageKey, params string[] messageArgs) {
            if (parent.Visible && PawnUtility.ShouldSendNotificationAbout(Pawn)) {
                Messages.Message(messageKey.Translate(messageArgs).CapitalizeFirst(), Pawn, MessageTypeDefOf.NegativeHealthEvent);
            }
            ((Hediff_Pregnancy)parent).Miscarry(baby);
        }

        /// <summary>
        /// Force a miscarriage of the current pregnancy, providing a message explaining why the pregnancy failed.
        /// The baby to be terminated is not a parameter here as we only want this comp and hediff to determine which
        /// pawn fails to be born.
        /// </summary>
        /// <param name="messageKey">a message bundle key to provide the translated message</param>
        /// <param name="messageArgs">a collection of arguments to be passed for formatting the provided message</param>
        public void Miscarry(string messageKey, params string[] messageArgs) {
            foreach (Pawn baby in ((Hediff_Pregnancy)parent).Babies) {
                CLog.Message("Causing miscarriage of unborn baby " + baby.Name);
                Miscarry(baby, messageKey, messageArgs);
                ((Hediff_Pregnancy)parent).Destroy();
            }
        }

        /// <summary>
        /// Determines if the pawn should miscarry due to starvation
        /// </summary>
        /// <returns>true if the pawn qualifies for miscarry, false if not or if starvation miscarry is disabled</returns>
        protected virtual bool ShouldStarvationMiscarry() {
            return Props.starvationCauseMiscarriage
                && Pawn.needs.food != null
                && Pawn.needs.food.CurCategory == HungerCategory.Starving
                && Rand.MTBEventOccurs(0.5f, GenDate.TicksPerDay, MiscarryCheckInterval);
        }

        /// <summary>
        /// Determines if the pawn should miscarry due to excessive wounds
        /// </summary>
        /// <returns>true if the pawn qualifies for miscarry, false if not or if wound miscarry is disabled</returns>
        protected virtual bool ShouldWoundsMiscarry() {
            return Props.woundsCauseMiscarriage
                && IsSeverelyWounded
                && Rand.MTBEventOccurs(0.5f, GenDate.TicksPerDay, MiscarryCheckInterval);
        }

        /// <summary>
        /// Determines if the pawn should miscarry due to other severe hediffs
        /// </summary>
        /// <param name="cause">an output parameter to store the hediff that caused the miscarriage, or null if none exist</param>
        /// <returns>true if the pawn qualifies for miscarry, false if not or if no hediffs are configured</returns>
        protected virtual bool ShouldHediffMiscarry(out Hediff cause) {
            if (Props.hediffsCauseMiscarriage != null) {
                List<Hediff> dangerousHediffs = Pawn.health.hediffSet.hediffs.FindAll(hediff => Props.hediffsCauseMiscarriage.Exists(he => he.hediffDef == hediff.def.defName));
                foreach (Hediff hediff in dangerousHediffs) {
                    if (hediff.Severity >= Props.hediffsCauseMiscarriage.Find(he => he.hediffDef == hediff.def.defName).minSeverity && Rand.MTBEventOccurs(0.5f, GenDate.TicksPerDay, MiscarryCheckInterval)) {
                        cause = hediff;
                        return true;
                    }
                }
            }
            cause = null;
            return false;
        }

        public override void CompPostTick(ref float severityAdjustment) {
            if (Pawn.IsHashIntervalTick(1000)) {
                Hediff hediff;
                if (ShouldHediffMiscarry(out hediff)) {
                    CLog.Message("Should miscarry due to hediff");
                    Miscarry("MessageMiscarriedHediff", Pawn.LabelIndefinite(), hediff.def.label);
                }

                if (ShouldStarvationMiscarry()) {
                    CLog.Message("Should miscarry due to starvation");
                    Miscarry("MessageMiscarriedStarvation", Pawn.LabelIndefinite());
                }

                if (ShouldWoundsMiscarry()) {
                    CLog.Message("Should miscarry due to poor health");
                    Miscarry("MessageMiscarriedPoorHealth", Pawn.LabelIndefinite());
                }
            }
        }
    }

    public class CompProperties_Miscarriable : HediffCompProperties {
        public bool woundsCauseMiscarriage = true;
        public bool starvationCauseMiscarriage = true;
        public List<MiscarryableHediff> hediffsCauseMiscarriage;

        public CompProperties_Miscarriable() {
            compClass = typeof(CompMiscarriable);
        }
    }
}
