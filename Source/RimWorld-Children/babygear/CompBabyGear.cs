﻿namespace RimWorldChildren.Babygear {
    using Verse;

    public class CompBabyGear : ThingComp
    {
        public CompProperties_BabyGear Props
        {
            get
            {
                return (CompProperties_BabyGear)this.props;
            }
        }
        public CompBabyGear()
        {
        }
    }
}
