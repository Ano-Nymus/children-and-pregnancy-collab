﻿using RimWorld;
using Verse;
using Verse.AI;
using System.Reflection;
using System.Collections.Generic;
using HugsLib;
using HugsLib.Settings;
using HarmonyLib;
using System.Reflection.Emit;
using System.Linq;
using System;
using RimWorldChildren.babygear;

namespace RimWorldChildren
{
	public class ChildrenBase : ModBase
	{
		public static ChildrenBase Instance { get; private set; }
		public override string ModIdentifier {
			get { return "Children_and_Pregnancy"; }
		}
		public SettingHandle<bool> pregnancy_disabled;
        public SettingHandle<int> gestation_days;
        public SettingHandle<float> impregnation_chance;
        public SettingHandle<int> max_traits;
        public SettingHandle<bool> debug_logging;
		public SettingHandle<bool> enable_postpartum;
		public SettingHandle<int> population_cap;
		public SettingHandle<int> random_traits;
        public SettingHandle<float> cryVolume;

        public static SettingHandle<bool> accelerated_growth;
		public static SettingHandle<int> accelerated_growth_end_age;
		public static SettingHandle<int> baby_accelerated_growth;
		public static SettingHandle<int> toddler_accelerated_growth;
		public static SettingHandle<int> child_accelerated_growth;


		public override void DefsLoaded() {
			bool GrowthVisible() => accelerated_growth;

			pregnancy_disabled = Settings.GetHandle<bool>("pregnancy_disabled", "PregnancyDisabled_title".Translate(), "PregnancyDisabled_desc".Translate(), false, null);
			gestation_days = Settings.GetHandle<int>("gestation_days", "GestateDaysSetting_title".Translate(), "GestateDaysSetting_desc".Translate(), 45, Validators.IntRangeValidator(15,60));
			impregnation_chance = Settings.GetHandle<float>("impregnation_chance", "ImpregnationChanceSetting_title".Translate(), "ImpregnationChanceSetting_desc".Translate(), 0.33f, Validators.FloatRangeValidator(0.1f,1f));
            max_traits = Settings.GetHandle<int>("max_traits", "MaxTraitsSetting_title".Translate(), "MaxTraitsSetting_desc".Translate(), 3, Validators.IntRangeValidator(0, 15));
			random_traits = Settings.GetHandle<int>("random_traits", "RandTraitsSetting_title".Translate(), "RandTraitsSetting_desc".Translate(), 2, Validators.IntRangeValidator(0, 15));
			enable_postpartum = Settings.GetHandle<bool>("enable_postpartum", "EnablePostPartum_title".Translate(), "EnablePostPartum_desc".Translate(), true, null);
			population_cap = Settings.GetHandle<int>("population_cap", "PopulationCapSetting_title".Translate(), "PopulationCapSetting_desc".Translate(), 0, null);
			accelerated_growth = Settings.GetHandle<bool>("accelerated_growth", "AcceleratedGrowth_title".Translate(), "AcceleratedGrowth_desc".Translate(), false, null);
			accelerated_growth_end_age = Settings.GetHandle<int>("accelerated_growth_end_age", "AcceleratedGrowthEndAge_title".Translate(), "AcceleratedGrowthEndAge_desc".Translate(), 8, Validators.IntRangeValidator(0, 12));
			accelerated_growth_end_age.SpinnerIncrement = 1;
			accelerated_growth_end_age.VisibilityPredicate = GrowthVisible;
			baby_accelerated_growth = Settings.GetHandle<int>("baby_accelerated_growth", "BabyAcceleratedGrowth_title".Translate(), "BabyAcceleratedGrowth_desc".Translate(), 2, Validators.IntRangeValidator(1, 12));
			baby_accelerated_growth.SpinnerIncrement = 1;
			baby_accelerated_growth.VisibilityPredicate = GrowthVisible;
			toddler_accelerated_growth = Settings.GetHandle<int>("toddler_accelerated_growth", "ToddlerAcceleratedGrowth_title".Translate(), "ToddlerAcceleratedGrowth_desc".Translate(), 2, Validators.IntRangeValidator(1, 12));
			toddler_accelerated_growth.SpinnerIncrement = 1;
			toddler_accelerated_growth.VisibilityPredicate = GrowthVisible;
			child_accelerated_growth = Settings.GetHandle<int>("child_accelerated_growth", "ChildAcceleratedGrowth_title".Translate(), "ChildAcceleratedGrowth_desc".Translate(), 2, Validators.IntRangeValidator(1, 12));
			child_accelerated_growth.SpinnerIncrement = 1;
			child_accelerated_growth.VisibilityPredicate = GrowthVisible;
			debug_logging = Settings.GetHandle<bool>("debug_logging", "DebugLogging_title".Translate(), "DebugLogging_desc".Translate(), false, null);

            cryVolume = Settings.GetHandle<float>("cryVolume", "CryVolumeSetting_title".Translate(), "CryVolumeSetting_desc".Translate(), 0.8f, Validators.FloatRangeValidator(0.0f, 1f));

        }
        public ChildrenBase() { Instance = this; }
	}

	// Handy for more readable code when age-checking
	public static class AgeStage
	{
		public const int Baby = 0;
		public const int Toddler = 1;
		public const int Child = 2;
		public const int Teenager = 3;
		public const int Adult = 4;
	}

	
	
	internal static class TranspilerHelper{

        internal static List<ThingStuffPair> FilterChildWeapons(Pawn pawn, List<ThingStuffPair> weapons) {
            var weapons_out = new List<ThingStuffPair>();
            if (weapons.Count > 0)
                foreach (ThingStuffPair weapon in weapons) {
                    if (weapon.thing.BaseMass < ChildrenUtility.ChildMaxWeaponMass(pawn)) {
                        weapons_out.Add(weapon);
                    }
                }
            return weapons_out;
        }

        internal static bool RecipeHasNoIngredients(RecipeDef recipe) {
            return recipe.ingredients.Count == 0;
        }


        [HarmonyPatch(typeof(JobGiver_OptimizeApparel), "TryGiveJob")]
		static class JobDriver_OptimizeApparel_Patch
		{
			[HarmonyPostfix]
			static void TryGiveJob_Patch(JobGiver_OptimizeApparel __instance, ref Job __result, Pawn pawn)
			{
				if (__result != null) {
					// Stop the game from automatically allocating pawns Wear jobs they cannot fulfil
					if ((ChildrenUtility.GetAgeStage(pawn) <= AgeStage.Toddler && __result.targetA.Thing.TryGetComp<CompBabyGear>() == null && ChildrenUtility.RaceUsesChildren(pawn))
							|| (ChildrenUtility.GetAgeStage(pawn) >= AgeStage.Child && __result.targetA.Thing.TryGetComp<CompBabyGear>() != null)) {
						__result = null;
					}
				} else {
					// makes pawn to remove baby clothes when too old for them.
					List<Apparel> wornApparel = pawn.apparel.WornApparel;
					if (ChildrenUtility.GetAgeStage(pawn) >= AgeStage.Child) {
						for (int i = wornApparel.Count - 1; i >= 0; i--) {
							CompBabyGear compBabyGear = wornApparel[i].TryGetComp<CompBabyGear>();
							if (compBabyGear != null) {
								__result = new Job(JobDefOf.RemoveApparel, wornApparel[i]) {
									haulDroppedApparel = true
								};
								return;
							}
						}
					}
				}
			}
		}
	}

	// Children are downed easier
	[HarmonyPatch(typeof(Pawn_HealthTracker), "ShouldBeDowned")]
	public static class Pawn_HealtherTracker_ShouldBeDowned_Patch
	{
		[HarmonyPostfix]
		internal static void SBD(ref Pawn_HealthTracker __instance, ref bool __result){
			Pawn pawn = (Pawn)AccessTools.Field (typeof(Pawn_HealthTracker), "pawn").GetValue(__instance);
			if (pawn.RaceProps.Humanlike && ChildrenUtility.GetAgeStage(pawn) <= 2) {
				__result = __instance.hediffSet.PainTotal > 0.4f || !__instance.capacities.CanBeAwake || !__instance.capacities.CapableOf (PawnCapacityDefOf.Moving);
			}
		}
	}

	// Stops pawns from randomly dying when downed
	// This is a necessary patch for preventing children from dying on pawn generation unfortunately
	[HarmonyPatch(typeof(Pawn_HealthTracker), "CheckForStateChange")]
	public static class Pawn_HealthTracker_CheckForStateChange_Patch
	{
		[HarmonyTranspiler]
		static IEnumerable<CodeInstruction> CheckForStateChange_Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			List<CodeInstruction> ILs = instructions.ToList ();

			MethodInfo isPlayerFaction = typeof(Faction).GetProperty ("IsPlayer").GetGetMethod ();
			int index = ILs.FindIndex (IL => IL.opcode == OpCodes.Callvirt && IL.operand == isPlayerFaction) + 2;
			List<CodeInstruction> injection = new List<CodeInstruction> {
				new CodeInstruction(OpCodes.Br, ILs[index-1].operand),
			};
			ILs.InsertRange (index, injection);

			foreach (CodeInstruction instruction in ILs) {
				yield return instruction;
			}
		}
	}

	

	// Causes children to drop too-heavy weapons and potentially hurt themselves on firing
	[HarmonyPatch(typeof(Verb_Shoot), "TryCastShot")]
	public static class VerbShoot_TryCastShot_Patch{
		[HarmonyPostfix]
		internal static void TryCastShot_Patch(ref Verb_Shoot __instance){
			Pawn pawn = __instance.CasterPawn;
			if (pawn != null && ChildrenUtility.RaceUsesChildren(pawn) && ChildrenUtility.GetAgeStage(pawn) <= AgeStage.Child) {
				// The weapon is too heavy and the child will (likely) drop it when trying to fire
				if (__instance.EquipmentSource.def.BaseMass > ChildrenUtility.ChildMaxWeaponMass(pawn)) {

					ThingWithComps benis;
					pawn.equipment.TryDropEquipment (__instance.EquipmentSource, out benis, pawn.Position, false);

					float recoilForce = (__instance.EquipmentSource.def.BaseMass - 3);

					if(recoilForce > 0){
						string[] hitPart = {
							"Torso",
							"Shoulder",
							"Arm",
							"Hand",
							"Head",
							"Neck",
							"Eye",
							"Nose", 
						};
						int hits = Rand.Range (1, 4);
						while (hits > 0) {
							pawn.TakeDamage (new DamageInfo (DamageDefOf.Blunt, (int)((recoilForce + Rand.Range (0f, 3f)) / hits), 0, -1, __instance.EquipmentSource,
								ChildrenUtility.GetPawnBodyPart (pawn, hitPart.RandomElement<String> ()), null));
							hits--;
						}
					}
				}
			}
		}
	}

	// Gives a notification that the weapon a child has picked up is dangerous for them to handle
	[HarmonyPatch(typeof(Pawn_EquipmentTracker), "Notify_EquipmentAdded")]
	public static class PawnEquipmentracker_NotifyEquipmentAdded_Patch{
		[HarmonyPostfix]
		internal static void Notify_EquipmentAdded_Patch(ref ThingWithComps eq, ref Pawn_EquipmentTracker __instance){
			Pawn pawn = __instance.ParentHolder as Pawn;
			if (pawn != null && ChildrenUtility.RaceUsesChildren(pawn) && eq.def.BaseMass > ChildrenUtility.ChildMaxWeaponMass(pawn) && ChildrenUtility.GetAgeStage(pawn) <= AgeStage.Child && pawn.Faction.IsPlayer) {
                Messages.Message("MessageWeaponTooLarge".Translate(eq.def.label, ((Pawn)__instance.ParentHolder).Name.ToStringShort), MessageTypeDefOf.CautionInput);
            }
		}
	}
	
	// Prevents children from being spawned with weapons too heavy for them
	// (For example in raids a child might otherwise spawn with an auto-shotty and promptly drop it the first time
	// they fire it at you. Which would be silly.)
	[HarmonyPatch(typeof(PawnWeaponGenerator), "TryGenerateWeaponFor")]
	public static class PawnWeaponGenerator_TryGenerateWeaponFor_Patch
	{
		[HarmonyTranspiler]
		static IEnumerable<CodeInstruction> TryGenerateWeaponFor_Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			List<CodeInstruction> ILs = instructions.ToList ();

			int index = ILs.FindIndex (IL => IL.opcode == OpCodes.Ldfld && IL.operand.ToStringSafe().Contains("Pawn_EquipmentTracker")) - 1;
			
			MethodInfo giveChildWeapons = typeof(TranspilerHelper).GetMethod("FilterChildWeapons", AccessTools.all);
			var injection = new List<CodeInstruction> {
				new CodeInstruction(OpCodes.Ldarg_0),
				new CodeInstruction(OpCodes.Ldloc_2),
				new CodeInstruction(OpCodes.Callvirt, giveChildWeapons),
				new CodeInstruction(OpCodes.Stloc_2),
			};
			ILs.InsertRange (index, injection);

			foreach (CodeInstruction instruction in ILs)
				yield return instruction;
		}
	}
	
	// Fixes null reference exception error if a Bill_Medical has no actual ingredients
	// Remove this code when it is fixed in Vanilla
	[HarmonyPatch(typeof(Bill_Medical), "Notify_DoBillStarted")]
	public static class Bill_Medical_NotifyDoBillStarted_Patch
	{
		[HarmonyTranspiler]
		static IEnumerable<CodeInstruction> Notify_DoBillStarted_Transpiler(IEnumerable<CodeInstruction> instructions, ILGenerator ILgen)
		{
            List<CodeInstruction> ILs = instructions.ToList();
            // Add the "jumpToEnd" label onto the last instruction
            Label jumpToEnd = ILgen.DefineLabel();
            ILs.Last().labels.Add(jumpToEnd);
            int index = ILs.FindIndex (IL => IL.opcode == OpCodes.Brtrue) + 1;
			var injection = new List<CodeInstruction> {
				new CodeInstruction(OpCodes.Ldarg_0),
				new CodeInstruction(OpCodes.Ldfld, typeof(Bill_Medical).GetField("recipe", AccessTools.all)),
				new CodeInstruction(OpCodes.Call, typeof(TranspilerHelper).GetMethod("RecipeHasNoIngredients", AccessTools.all)),
				new CodeInstruction(OpCodes.Brtrue, jumpToEnd),
			};
			ILs.InsertRange (index, injection);
			foreach (CodeInstruction instruction in ILs)
				yield return instruction;
		}
	}


	
    /// <summary>
    /// TODO: Document
    /// Why is this patch necessary?
    /// </summary>
	[HarmonyPatch(typeof(WorkGiver_Warden_DeliverFood), "FoodAvailableInRoomTo")]
	public static class WGWDF_FAIRT_Patch{
		[HarmonyTranspiler]
		static IEnumerable<CodeInstruction> _Transpiler(IEnumerable<CodeInstruction> insts){
			foreach(CodeInstruction inst in insts){
				if(inst.opcode == OpCodes.Ldc_R4 && (float)(inst.operand) == 0.5f)
					yield return new CodeInstruction(OpCodes.Ldc_R4, 0.01f);
				else
					yield return inst;
			}
		}
	}
	
	
}

