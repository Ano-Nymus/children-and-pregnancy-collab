﻿namespace RimWorldChildren {
    using System.Collections.Generic;
    using RimWorld;
    using Verse;

    public class Recipe_Abortion : Recipe_RemoveHediff
    {
        public override void ApplyOnPawn (Pawn pawn, BodyPartRecord part, Pawn billDoer, List<Thing> ingredients, Bill bill)
        {
            // We don't check if the surgery fails, because even if the surgery is a failure the baby will still die

            if (billDoer != null) {

                TaleRecorder.RecordTale (TaleDefOf.DidSurgery, new object[] {
                    billDoer,
                    pawn
                });
                if (base.CheckSurgeryFail (billDoer, pawn, ingredients, part, bill) == false){
                    if (PawnUtility.ShouldSendNotificationAbout (pawn) || PawnUtility.ShouldSendNotificationAbout (billDoer)) {
                        string text;
                        if (!this.recipe.successfullyRemovedHediffMessage.NullOrEmpty ()) {
                            text = string.Format (this.recipe.successfullyRemovedHediffMessage, billDoer.LabelShort, pawn.LabelShort);
                        }
                        else {
                            text = "MessageSuccessfullyRemovedHediff".Translate(
                                billDoer.LabelShort,
                                pawn.LabelShort,
                                this.recipe.removesHediff.label
                            );
                        }
                        Messages.Message (text, pawn, MessageTypeDefOf.TaskCompletion);
                    }
                }
            }

            foreach (string hediffDefName in Hediff_Pregnancy.HediffOfClass.Values) {
                Hediff_Pregnancy preggo = (Hediff_Pregnancy)pawn.health.hediffSet.GetFirstHediffOfDef(HediffDef.Named(hediffDefName));
                if (HediffDef.Named(hediffDefName).HasComp(typeof(CompAbortable))) {
                    CLog.Message("Attempting abortion for pregnancy " + hediffDefName);
                    CompAbortable abortable = preggo.TryGetComp<CompAbortable>();
                    abortable.Abort(null);
                }
            }
        }
    }
}