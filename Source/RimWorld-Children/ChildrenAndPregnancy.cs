﻿namespace RimWorldChildren {
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    using Verse;

    /// <summary>
    /// Mod definition class
    /// </summary>
    public class ChildrenAndPregnancy : Mod {
        public static CnpSettings Settings;
        private int curTab;
        private Vector2 scroll = new Vector2(0, 0);
        private Rect viewRect = new Rect();

        private string[] buffers = new string[10];

        public ChildrenAndPregnancy(ModContentPack content) : base(content) {
            Settings = GetSettings<CnpSettings>();

            // For some reason, StaticConstructorOnStartup on a regular class and StaticConstructorOnStartup on a subclass are executed at different times
            // When used on a subclass, it is executed before defs are loaded. LongEventHandler will force us to wait before loading the settings
            LongEventHandler.QueueLongEvent(delegate { Settings.Initialize(); }, "Initializing", true, null);
        }

        public override void DoSettingsWindowContents(Rect inRect) {
            Rect menuRect = inRect;
            menuRect.y += 35f;
            inRect.height -= 35f;
            inRect.y += 35f;
            Widgets.DrawMenuSection(inRect);

            List<TabRecord> menuTabs = new List<TabRecord>() {
                 new TabRecord("MenuMainTab".Translate(), () => { curTab = 0; }, curTab == 0),
                 new TabRecord("MenuRaceTab".Translate(), () => { curTab = 1; }, curTab == 1),
            };

            TabDrawer.DrawTabs(menuRect, menuTabs);
            switch (curTab) {
                case 0: DoMainTabContents(menuRect); break;
                case 1: DoRaceTabContents(menuRect); break;
            }
        }

        /// <summary>
        /// Window contents displayed when user has selected the Main settings tab
        /// </summary>
        /// <param name="menu">Rect to display tab contents in</param>
        private void DoMainTabContents(Rect menu) {
            Listing_Standard list = new Listing_Standard();
            list.BeginScrollView(
                new Rect(menu.x + 10, menu.y, menu.width, menu.height - 40),
                ref scroll,
                ref viewRect);
            list.ColumnWidth = menu.width * 0.33f;

            // Do some jenky stuff with an array to avoid having to declare a dozen buffer variables
            int bufferIndex = -1;
            list.Label("MaxTraitsSetting_title".Translate(), -1, "MaxTraitsSetting_desc".Translate());
            list.TextFieldNumeric(ref Settings.maxTraits, ref buffers[++bufferIndex], 0, 15);
            list.Label("RandTraitsSetting_title".Translate(), -1, "RandTraitsSetting_desc".Translate());
            list.TextFieldNumeric(ref Settings.randTraits, ref buffers[++bufferIndex], 0, 3);
            list.CheckboxLabeled("DebugLogging_title".Translate(), ref Settings.debugLogging, "DebugLogging_desc".Translate());
            list.Label("PopulationCapSetting_title".Translate(), -1, "PopulationCapSetting_desc".Translate());
            list.TextFieldNumeric(ref Settings.populationCap, ref buffers[++bufferIndex], 0, 100);

            list.Label("CryVolumeSetting_title".Translate(), -1, "CryVolumeSetting_desc".Translate());
            Settings.cryVolume = list.Slider(Settings.cryVolume, 0.0f, 1.0f);

            list.CheckboxLabeled("AcceleratedGrowth_title".Translate(), ref Settings.acceleratedGrowthEnabled, "AcceleratedGrowth_desc".Translate());
            if (Settings.acceleratedGrowthEnabled) {
                list.Label("AcceleratedGrowthEndAge_title".Translate(), -1, "AcceleratedGrowthEndAge_desc".Translate());
                list.TextFieldNumeric(ref Settings.acceleratedGrowthEndAge, ref buffers[++bufferIndex], 0, 12);
                list.Label("BabyAcceleratedGrowth_title".Translate(), -1, "BabyAcceleratedGrowth_desc".Translate());
                list.TextFieldNumeric(ref Settings.babyAccelerationFactor, ref buffers[++bufferIndex], 1, 12);
                list.Label("ToddlerAcceleratedGrowth_title".Translate(), -1, "ToddlerAcceleratedGrowth_desc".Translate());
                list.TextFieldNumeric(ref Settings.toddlerAccelerationFactor, ref buffers[++bufferIndex], 1, 12);
                list.Label("ChildAcceleratedGrowth_title".Translate(), -1, "ChildAcceleratedGrowth_desc".Translate());
                list.TextFieldNumeric(ref Settings.childAccelerationFactor, ref buffers[++bufferIndex], 1, 12);
            }

            if (Settings.debugLogging && list.ButtonText("Alignment Testing Tool")) {
                Find.WindowStack.Add(new AlignmentUtilityWindow());
            }

            list.EndScrollView(ref viewRect);
        }

        /// <summary>
        /// Window contents displayed when user has selected the Race settings tab
        /// </summary>
        /// <param name="menu">Rect to display tab contents in</param>
        private void DoRaceTabContents(Rect menu) {
            Listing_Standard list = new Listing_Standard();
            viewRect = new Rect(menu.x - 20, menu.y - 20, menu.width - 40, (Settings.racialSettings.Count * 30) - 60);
            list.BeginScrollView(
                new Rect(menu.x + 10, menu.y, menu.width - 20, menu.height - 40),
                ref scroll,
                ref viewRect);
            list.ColumnWidth = menu.width - 20;
            float y = menu.y;
            int index = 0;
            foreach (string race in Settings.racialSettings.Keys) {
                y = index * 30;
                ++index;
                Widgets.Label(new Rect(0, y, 100, 22), race + ":");
                Widgets.Label(new Rect(100, y, 80, 22), "PregnancyEnabled_title".Translate() + ":");
                Widgets.Checkbox(new Vector2(180, y - 1), ref Settings.racialSettings[race].pregnancyEnabled);
                Widgets.Label(new Rect(210, y, 60, 22), "LifecycleEnabled_title".Translate() + ":");
                Widgets.Checkbox(new Vector2(280, y - 1), ref Settings.racialSettings[race].lifeCycleEnabled);
            }

            list.EndScrollView(ref viewRect);
        }

        public override string SettingsCategory() {
            return "ModTitle".Translate();
        }
    }
}