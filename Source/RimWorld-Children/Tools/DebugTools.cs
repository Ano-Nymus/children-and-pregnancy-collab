﻿namespace RimWorldChildren {
    using System.Collections.Generic;
    using System.Linq;
    using RimWorld;
    using Verse;
    using Verse.AI;

    // Makes the pawn do Lovin' if they're in bed with their partner
    public class Hediff_GetFuckin : HediffWithComps {
        public override void PostMake() {
            base.PostMake ();
            if (pawn.InBed ()) {
                Pawn partner = LovePartnerRelationUtility.GetPartnerInMyBed (pawn);
                Building_Bed bed = pawn.CurrentBed();
                if (partner != null) {
                    Job lovin = new Job (JobDefOf.Lovin, partner, bed);
                    pawn.jobs.StartJob (lovin, JobCondition.InterruptForced, null, false, true, null);
                }
            }
            pawn.health.RemoveHediff (this);
            return;
        }
    }

    // Makes the pawn pregnant (if not already) and sets the pregnancy to near its end
    public class Hediff_MakePregnateLate : HediffWithComps {
        public override void Tick () {
            if (!pawn.health.hediffSet.HasHediff(HediffDef.Named("HumanPregnancy"))) {
                pawn.health.AddHediff (Hediff_Pregnancy.Create<Hediff_HumanPregnancy>(pawn, null), pawn.RaceProps.body.AllParts.Find(x => x.def == BodyPartDefOf.Torso), null);
            }
            pawn.health.hediffSet.GetFirstHediffOfDef (HediffDef.Named("HumanPregnancy")).Severity = 0.995f;

            pawn.health.RemoveHediff (this);
        }
    }

    // Play with baby
    public class Hediff_GiveJobTest : HediffWithComps {
        public override void Tick() {
            Pawn baby = null;
            // Try to find a baby
            foreach(Pawn colonist in pawn.Map.mapPawns.FreeColonists){
                if (ChildrenUtility.GetLifestageType(colonist) <= LifestageType.Baby) {
                    baby = colonist;
                }
            }
            if (baby != null) {
                Job playJob = new Job(DefDatabase<JobDef>.GetNamed("BreastFeedBaby"), baby);
                // pawn.QueueJob (playJob);
                pawn.jobs.StartJob(playJob, JobCondition.InterruptForced, null, true, true, null, null);
                Log.Message("Found baby " + baby.Name.ToStringShort + " and proceeding to breastfeed.");
            }
            else {
                Log.Message("Failed to find any baby.");
            }

            pawn.health.RemoveHediff (this);
        }
    }

    public static class DebugToolsChildren {
        [DebugAction("Pawns", null, actionType = DebugActionType.ToolMap, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        private static void AdvancePregnancy() {
            foreach (Thing thing in UI.MouseCell().GetThingList(Find.CurrentMap).ToList<Thing>()) {
                Pawn pawn = thing as Pawn;
                if (pawn != null) {
                    Hediff preg = pawn.health.hediffSet.GetFirstHediffOfDef(HediffDef.Named("HumanPregnancy"));
                    if (preg != null) {
                        if(preg.Severity < .66f)
                        {
                            preg.Severity += .33f;
                        }
                        else if (preg.Severity < .75f)
                        {
                            preg.Severity = .76f;
                        }
                        else
                        {
                            preg.Severity = .99f;
                        }
                    }

                }
            }
        }

        [DebugAction("Spawning", null, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        private static void SpawnBaby() {
            List<DebugMenuOption> debugMenuOptionList = new List<DebugMenuOption>();
            foreach (PawnKindDef pawnKindDef in DefDatabase<PawnKindDef>.AllDefs.Where(kd => ChildrenUtility.RaceUsesChildren(kd.race.defName))) {
                debugMenuOptionList.Add(new DebugMenuOption(pawnKindDef.defName, DebugMenuOptionMode.Tool, () => {
                    Faction faction = FactionUtility.DefaultFactionFrom(pawnKindDef.defaultFactionType);
                    PawnGenerationRequest request = new PawnGenerationRequest(pawnKindDef, faction, PawnGenerationContext.NonPlayer, Find.CurrentMap.Tile, true, true, false, false, false, false, 0f, false, true, false, false, false, false, false, false, 0f, null, 0f, null, null, null, null, 0f, 0f, 0f, default(Gender?), 0, "M", null, null);
                    Pawn newPawn = PawnGenerator.GeneratePawn(request);
                    LifecycleComp comp = newPawn.TryGetComp<LifecycleComp>();
                    comp.Props.ColonyBorn = true;
                    comp.Initialize(true);
                    GenSpawn.Spawn((Thing)newPawn, UI.MouseCell(), Find.CurrentMap, WipeMode.Vanish);
                }));
            }

            Find.WindowStack.Add(new Dialog_DebugOptionListLister(debugMenuOptionList));
        }
    }
}