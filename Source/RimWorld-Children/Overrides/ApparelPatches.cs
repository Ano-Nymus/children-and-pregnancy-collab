﻿namespace RimWorldChildren {
    using System.Collections.Generic;
    using HarmonyLib;
    using RimWorld;
    using RimWorldChildren.Babygear;
    using Verse;
    using Verse.AI;

    /// <summary>
    /// Harmony patches for classes relateed to apparel, clothing, and jobdrivers
    /// </summary>
    public class OptimizeApparelPatches {
        [HarmonyPatch(typeof(JobGiver_OptimizeApparel), "TryGiveJob")]
        public static class JobDriver_OptimizeApparel_Patch {
            [HarmonyPostfix]
            public static void TryGiveJob_Patch(JobGiver_OptimizeApparel __instance, ref Job __result, Pawn pawn) {
                if (__result != null) {
                    // Stop the game from automatically allocating pawns Wear jobs they cannot fulfil
                    if ((ChildrenUtility.GetLifestageType(pawn) <= LifestageType.Toddler && __result.targetA.Thing.TryGetComp<CompBabyGear>() == null && ChildrenUtility.RaceUsesChildren(pawn))
                            || (ChildrenUtility.GetLifestageType(pawn) >= LifestageType.Child && __result.targetA.Thing.TryGetComp<CompBabyGear>() != null)) {
                        __result = null;
                    }
                }
                else {
                    // makes pawn to remove baby clothes when too old for them.
                    List<Apparel> wornApparel = pawn.apparel.WornApparel;
                    if (ChildrenUtility.GetLifestageType(pawn) >= LifestageType.Child) {
                        for (int i = wornApparel.Count - 1; i >= 0; i--) {
                            CompBabyGear compBabyGear = wornApparel[i].TryGetComp<CompBabyGear>();
                            if (compBabyGear != null) {
                                __result = new Job(JobDefOf.RemoveApparel, wornApparel[i]) {
                                    haulDroppedApparel = true,
                                };
                                return;
                            }
                        }
                    }
                }
            }
        }
    }

    // Prevents babies from wearing normal clothes
    // Needs to be expanded upon when toddler clothing is added
    [HarmonyPatch(typeof(JobDriver_Wear), "MakeNewToils")]
    internal static class Wear_Override {
        [HarmonyPrefix]
        internal static void JobDriver_Wear_Patch(ref JobDriver_Wear __instance) {
            Pawn actor = __instance.GetActor();
            Job job = __instance.job;
            __instance.FailOn(delegate {
                // TODO: Revisit to handle different types of gear comps per race
                if (actor.TryGetComp<LifecycleComp>()?.CurrentLifestageIndex <= 1 && job.GetTarget(TargetIndex.A).Thing.TryGetComp<CompBabyGear>() == null && ChildrenUtility.RaceUsesChildren(actor)) {
                    Messages.Message("MessageAdultClothesOnBaby".Translate(actor.Name.ToStringShort), actor, MessageTypeDefOf.CautionInput);
                    return true;
                }

                // tried testing for bool "isBabyGear" but that caused a null error exception, so I switched to just testing if comp was null, will look into later
                if (actor.TryGetComp<LifecycleComp>()?.CurrentLifestageIndex > 1 && job.GetTarget(TargetIndex.A).Thing.TryGetComp<CompBabyGear>() != null) {
                    Messages.Message("MessageBabyClothesOnAdult".Translate(actor.LabelShort), actor, MessageTypeDefOf.CautionInput);
                    return true;
                }
                else {
                    return false;
                }
            });
        }
    }
}