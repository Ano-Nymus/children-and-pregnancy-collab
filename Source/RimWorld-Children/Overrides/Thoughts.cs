﻿namespace RimWorldChildren {
    using HarmonyLib;
    using RimWorld;
    using Verse;
    using Verse.AI;

    // Creates a blacklist of thoughts Toddlers cannot have
    [HarmonyPatch(typeof(ThoughtUtility), "CanGetThought")]
    public static class ThoughtUtility_CanGetThought_Patch{
        /// <summary>
        /// Prevent the acquisition of certain thoughts based on the life stage of this pawn
        /// </summary>
        [HarmonyPostfix]
        internal static void CanGetThought_Patch(ref Pawn pawn, ref ThoughtDef def, ref bool __result) {
            LifecycleComp comp = pawn.TryGetComp<LifecycleComp>();
            if (!comp?.CurrentLifestage?.cannotReceiveThoughts?.NullOrEmpty() ?? false) {
                __result = __result && !comp.CurrentLifestage.cannotReceiveThoughts.Contains(def);
            }
        }
    }

    // Reroutes social fighting to account for children
    [HarmonyPatch(typeof(JobGiver_SocialFighting), "TryGiveJob")]
    public static class JobGiver_SocialFighting_TryGiveJob_Patch {
        [HarmonyPostfix]
        internal static void TryGiveJob_Postfix(ref Pawn pawn, ref Job __result){
            Pawn other = ((MentalState_SocialFighting)pawn.MentalState).otherPawn;
            if (__result != null) {
                // Make sure kids don't start social fights with adults
                if (ChildrenUtility.GetLifestageType(other) > LifestageType.Child && ChildrenUtility.GetLifestageType(pawn) <= LifestageType.Child) {
                    CLog.Message ("Child starting social fight with adult");

                    // Adult will "start" the fight, following the code below
                    other.interactions.StartSocialFight (pawn);
                    __result = null;
                }

                // Make sure adults don't start social fights with kids (unless psychopaths)
                if (ChildrenUtility.GetLifestageType(other) <= LifestageType.Child && ChildrenUtility.GetLifestageType(pawn) > LifestageType.Child && !pawn.story.traits.HasTrait(TraitDefOf.Psychopath)) {
                    // If the pawn is not in a bad mood or is kind, they'll just tell them off
                    if (pawn.story.traits.HasTrait (TraitDefOf.Kind) || pawn.needs.mood.CurInstantLevel > 0.45f || pawn.WorkTagIsDisabled(WorkTags.Violent)) {
                        JobDef chastise = DefDatabase<JobDef>.GetNamed("ScoldChild", true);
                        __result = new Job (chastise, other);
                    }

                    // Otherwise the adult will smack the child around
                    else if (other.health.summaryHealth.SummaryHealthPercent > 0.93f) {
                        JobDef paddlin = DefDatabase<JobDef>.GetNamed("DisciplineChild", true);
                        __result = new Job(paddlin, other);
                    }

                    pawn.MentalState.RecoverFromState();
                    __result = null;
                }
            }
        }
    }
}