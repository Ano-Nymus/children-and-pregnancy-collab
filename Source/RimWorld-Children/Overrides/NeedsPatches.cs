﻿namespace RimWorldChildren {
    using System.Collections.Generic;
    using System.Reflection;
    using System.Reflection.Emit;
    using HarmonyLib;
    using RimWorld;
    using Verse;

    /// <summary>
    /// Patches for classes related to pawn needs, need fulfillment, etc
    /// </summary>
    public class NeedsPatches {
        /// <summary>
        /// Babies should not develop a tolerance for social joy since this is currently their only available joy source.
        /// </summary>
        [HarmonyPatch(typeof(Need_Joy), "GainJoy")]
        public static class NeedJoy_GainJoy_Patch {
            [HarmonyPostfix]
            internal static void GainJoy_Patch(Need_Joy __instance, JoyKindDef joyKind, Pawn ___pawn) {
                if (ChildrenUtility.RaceUsesChildren(___pawn) && joyKind == JoyKindDefOf.Social && ChildrenUtility.GetLifestageType(___pawn) <= LifestageType.Toddler) {
                    DefMap<JoyKindDef, float> tolerances = Traverse.Create(__instance).Field("tolerances").Field("tolerances").GetValue<DefMap<JoyKindDef, float>>();
                    DefMap<JoyKindDef, bool> bored = Traverse.Create(__instance).Field("tolerances").Field("bored").GetValue<DefMap<JoyKindDef, bool>>();
                    tolerances[JoyKindDefOf.Social] = 0;
                    bored[JoyKindDefOf.Social] = false;
                }
            }
        }

        /// <summary>
        /// Get the most appropriate bed list for a pawn. Child pawns will recieve
        /// a bed list sorted to prioritize cribs
        /// </summary>
        /// <param name="pawn">The pawn being evaluated</param>
        /// <returns>Sorted list of beds </returns>
        private static List<ThingDef> GetSortedBeds_RestEffectiveness(Pawn pawn) {
            return ChildrenUtility.ShouldUseCrib(pawn) ? ChildrenUtility.AllBedDefBestToWorstCribRest : RestUtility.AllBedDefBestToWorst;
        }

        private static MethodInfo GetSortedBeds_MethodInfo = AccessTools.Method(typeof(NeedsPatches), nameof(GetSortedBeds_RestEffectiveness));

        // Babies wake up if they're unhappy
        [HarmonyPatch(typeof(RestUtility), "WakeThreshold")]
        public static class RestUtility_WakeThreshold_Patch {
            [HarmonyPrefix]
            internal static bool WakeThreshold_Patch(ref float __result, ref Pawn p) {
                if (p.TryGetComp<LifecycleComp>()?.CurrentLifestage?.wakeOnUnhappy ?? false) {
                    if (p.health.hediffSet.HasHediff(ChildHediffDefOf.UnhappyBaby)){
                        __result = 0.15f;
                        return false;
                    }
                }

                // Adults nearby wake up too
                if (ChildrenUtility.NearCryingBaby(p)) {
                    __result = 0.15f;
                    return false;
                }

                return true;
            }
        }

        [HarmonyPatch(typeof(RestUtility), "IsValidBedFor")]
        public static class RestUtility_IsValidBedFor_Patch {
            /// <summary>
            /// Failsafe check to ensure that any pawn looking for a bed will not find a crib if
            /// they do not belong in a crib
            /// </summary>
            [HarmonyPostfix]
            internal static void IsValidBedFor_Postfix(Pawn sleeper, Thing bedThing, bool __result) {
                if (ChildrenUtility.IsBedCrib((Building_Bed)bedThing) && !ChildrenUtility.ShouldUseCrib(sleeper)) {
                    __result = false;
                }
            }
        }

        [HarmonyPatch(typeof(RestUtility), "FindBedFor", new[] { typeof(Pawn), typeof(Pawn), typeof(bool), typeof(bool), typeof(bool) })]
        public static class RestUtility_FindBedFor_Patch {
            /// <summary>
            /// Modify FindBedFor to pass in a different order for bedDefs when looking
            /// for the best bed for a pawn. We do not touch the order of beds for
            /// medical effectiveness, as a baby still needs to go to the most effective
            /// hospital bed if actually unwell.
            /// </summary>
            [HarmonyTranspiler]
            public static IEnumerable<CodeInstruction> FindBedFor_Transpiler(IEnumerable<CodeInstruction> instructions) {
                FieldInfo bedDefFieldInfo = AccessTools.Field(typeof(RestUtility), "bedDefsBestToWorst_RestEffectiveness");
                foreach (CodeInstruction instruction in instructions) {
                    // Convert all references to bedDefsBestToWorst_RestEffectiveness for toddler pawns
                    if (instruction.opcode == OpCodes.Ldsfld && instruction.OperandIs(bedDefFieldInfo)) {
                        yield return new CodeInstruction(OpCodes.Ldarg_1) { labels = instruction.labels.ListFullCopy() };
                        yield return new CodeInstruction(OpCodes.Call, GetSortedBeds_MethodInfo);
                    }
                    else {
                        yield return instruction;
                    }
                }
            }
        }

        [HarmonyPatch(typeof(FoodUtility), "ShouldBeFedBySomeone")]
        public static class ShouldBeFedBySomeone_Patch {
            [HarmonyPostfix]
            internal static void ShouldBeFedBySomeone_Postfix(Pawn pawn, ref bool __result) {
                if (ChildrenUtility.ShouldBeFed(pawn)) {
                    __result = true;
                }
            }
        }

        [HarmonyPatch(typeof(WorkGiver_Warden_DeliverFood), "FoodAvailableInRoomTo")]
        public static class FoodAvailableInRoomTo_Patch {
            [HarmonyTranspiler]
            static IEnumerable<CodeInstruction> FoodAvailableInRoomTo_Transpiler(IEnumerable<CodeInstruction> insts) {
                foreach (CodeInstruction inst in insts) {
                    if (inst.opcode == OpCodes.Ldc_R4 && (float)inst.operand == 0.5f) {
                        yield return new CodeInstruction(OpCodes.Ldc_R4, 0.01f);
                    }
                    else {
                        yield return inst;
                    }
                }
            }
        }
    }
}
