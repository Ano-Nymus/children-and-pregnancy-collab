﻿namespace RimWorldChildren {
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using HarmonyLib;
    using RimWorld;
    using Verse;
    using Verse.AI;

    /// <summary>
    ///  Patches for classes related to lovin'
    /// </summary>
    [HarmonyPatch(typeof(JobDriver), "Cleanup")]
    internal static class Lovin_Override
    {
        [HarmonyPrefix]
        internal static bool JobDriver_Lovin_Patch(JobDriver __instance, JobCondition condition) {
            if (__instance == null) {
                return true;
            }

            if (__instance.GetType() == typeof(JobDriver_Lovin) && condition == JobCondition.Succeeded) {
                Pawn partner = (Pawn)__instance.GetType().GetProperty("Partner", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).GetValue(__instance, null);
                Pawn pawn = __instance.pawn;
                TryToImpregnate(pawn, partner);
            }

            return true;
        }

        internal static void TryToImpregnate(Pawn initiator, Pawn partner) {
            // Lesbian/gay couples. Those cases should never result in pregnancy
            if (initiator.gender == partner.gender) {
                return;
            }

            // Check if population limit applies
            if (ChildrenAndPregnancy.Settings.populationCap > 0 && PawnsFinder.AllMapsCaravansAndTravelingTransportPods_Alive_OfPlayerFaction_NoCryptosleep.Count >= ChildrenAndPregnancy.Settings.populationCap) {
                return;
            }

            // One of the two is sterile, so don't continue
            foreach (Pawn pawn in new List<Pawn> { initiator, partner }) {
                if (pawn.health.hediffSet.HasHediff(HediffDef.Named("Sterile"))) {
                    return;
                }
            }

            Pawn male = initiator.gender == Gender.Male ? initiator : partner;
            Pawn female = initiator.gender == Gender.Female ? initiator : partner;

            // Check Mod Settings to ensure pregnancy isn't disabled
            if (!ChildrenAndPregnancy.Settings.RacialSettings(female.def.defName).pregnancyEnabled) {
                return;
            }

            if (!female.Faction.IsPlayer) {
                // TODO: Revisit non player factions at a later time.
                CLog.Message("Skipping impregnation for pawn " + female.Name + " as they do not belong to the player faction");
                return;
            }

            // if woman is in postpartum, don't continue
            if (female.health.hediffSet.HasHediff(HediffDef.Named("PostPartum"))) {
                return;
            }

            LifecycleComp comp = female.TryGetComp<LifecycleComp>();

            if (comp == null) {
                CLog.Warning("Lovin' initiated with no Lifestage Comp associated.");
                return;
            }

            // Only humans can be impregnated for now
            if (!ChildrenUtility.RaceUsesChildren(female)) {
                return;
            }

            BodyPartRecord torso = female.RaceProps.body.AllParts.Find (x => x.def == BodyPartDefOf.Torso);
            HediffDef contraceptive = HediffDef.Named ("Contraceptive");

            // Make sure the woman is not pregnant and not using a contraceptive
            if (female.health.hediffSet.HasHediff(HediffDefOf.Pregnant, torso) || female.health.hediffSet.HasHediff(contraceptive, null) || male.health.hediffSet.HasHediff(contraceptive, null)) {
                CLog.Message("Skipping impregnation because " + female.Name + " is already pregnant, or one partner is using contraceptives");
                return;
            }

            // Check the pawn's age to see how likely it is she can carry a fetus
            // 25 and below is guaranteed, 50 and above is impossible, 37.5 is 50% chance
            float preg_chance = Math.Max (1 - (Math.Max (female.ageTracker.AgeBiologicalYearsFloat - 25, 0) / 25), 0) * female.TryGetComp<LifecycleComp>().LifecycleDef.impregnationChance;

            if (preg_chance < Rand.Value) {
                CLog.Message("Impregnation failed. Chance was " + preg_chance);
                return;
            }

            CLog.Message("Impregnation succeeded. Chance was " + preg_chance);

            // Spawn a bunch of hearts. Sharp eyed players may notice this means impregnation occurred.
            for (int i = 0; i <= 3; i++){
                MoteMaker.ThrowMetaIcon(male.Position, male.MapHeld, ThingDefOf.Mote_Heart);
                MoteMaker.ThrowMetaIcon(female.Position, male.MapHeld, ThingDefOf.Mote_Heart);
            }

            // Do the actual impregnation. We apply it to the torso because Remove_Hediff in operations doesn't work on WholeBody (null body part)
            // for whatever reason.
            female.health.AddHediff(Hediff_Pregnancy.Create<Hediff_HumanPregnancy>(female, male), torso);
        }
    }

    [HarmonyPatch(typeof(JobGiver_DoLovin), "TryGiveJob")]
    public static class JobGiver_DoLovin_TryGiveJob_Patch {
        [HarmonyPostfix]
        internal static void TryGiveJob_Patch(ref Job __result, ref Pawn pawn) {
            if (pawn.ageTracker != null && ChildrenUtility.GetLifestageType(pawn) <= LifestageType.Child) {
                __result = null;
            }
        }
    }
}