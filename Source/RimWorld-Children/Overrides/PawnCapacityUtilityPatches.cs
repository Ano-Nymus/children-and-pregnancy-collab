﻿namespace RimWorldChildren {
    using System.Collections.Generic;
    using HarmonyLib;
    using Verse;

    public class PawnCapacityUtilityPatches {
        [HarmonyPatch(typeof(PawnCapacityUtility), "CalculateCapacityLevel")]
        public static class CalculateCapacityLevel_Patch {
            /// <summary>
            /// Lazy way to inject our capmods without creating a transpiler.
            /// Create a fake hediff to pass our capmods to CalculateCapacityLevel, then cleanup when adjustments aren't needed
            /// </summary>
            [HarmonyPrefix]
            public static bool CalculateCapacityLevel_Prefix(HediffSet diffSet, PawnCapacityDef capacity) {
                LifecycleComp comp = diffSet.pawn.TryGetComp<LifecycleComp>();
                if (diffSet.pawn.Spawned && comp != null && comp.CurrentLifestage != null) {
                    Hediff fakeHediff = new Hediff();
                    fakeHediff.loadID = Find.UniqueIDsManager.GetNextHediffID();
                    HediffDef def = ChildHediffDefOf.LifestageCapModInjections;
                    HediffStage stage = new HediffStage();
                    stage.capMods = comp.CurrentLifestage.capMods;
                    stage.becomeVisible = false;
                    def.stages = new List<HediffStage>() { stage };
                    fakeHediff.def = def;
                    if (!stage.capMods.NullOrEmpty()) {
                        if (diffSet.hediffs.Find(x => x.def.defName == fakeHediff.def.defName) == null) {
                            diffSet.hediffs.Add(fakeHediff);
                        }
                        else {
                            diffSet.hediffs.Find(x => x.def.defName == fakeHediff.def.defName).def.stages = new List<HediffStage>() { stage };
                        }
                    }
                    else {
                        diffSet.hediffs.RemoveAll(h => h.def == fakeHediff.def);
                    }
                }

                return true;
            }
        }
    }
}
