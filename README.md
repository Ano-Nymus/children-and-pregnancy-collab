## Humanoid Alien Races
Support for alien races is provided through a Racial Lifecycle Def that defines how Children and Pregnancy should handle the race in terms of features and rendering.
A default def is provided that any enabled race will use until a race specific def is provided. Modders may provide a def to go along with their race so that CNP knows how to handle
race. The default def cannot handle all HAR body addons, so races that use addons will most likely need a custom def.

### Race Configuration  
**Def Resolution**  
In order to identify a race's life cycle def, we look at the defName. The appropriate name for resolution is {race defName}_Lifestages.
If a def with this name cannot be found, it will fall back to using Default_Lifestages. 

### Def Construction
It is assumed that lifestages will be listed **in order** from youngest to oldest. Violating this assumption will have undefined results.
There must be atleast one lifestage entry for each of a race's lifestages.

- **raceDefName** - In the form {race defName}_Lifestages, used to associate the def with a specific race
- **lifeStages** - (Optional) A collection of life stages each defining how to process a pawn at a specific development milestone. 
  - **lifeStageDef** - (Required) The associated LifestageDef, used to determine when to apply this life stage to the pawn.
  - **impregnationChance** - (Required) The base percentage chance of impregnation. Other factors apply.
  - **gestationDays** - (Required) The number of days from inception to birth.
  - **postpartumEnabled** - (Optional) Default: True. When enabled, there is a brief period in which a pawn may not become pregnant again after birth.
  - **lifestageType** - (Required) A broad stroke growth stage used to apply certain features that are not configurable. 
    - Unborn  
    - Newborn  
    - Baby  
    - Toddler  
    - Child  
    - Adolescent  
    - Adult  
    - Elder  
  - **minAge** - (Optional) Integer years. Used with lifeStageDef to narrow down when to apply this stage. Once the pawn reaches the define lifeStageDef, 
                            they must also reach this age before the stage will apply.
  - **maturity** - (Optional) Default: False. Juryrigged solution to define a specific development stage as "mature" during which any earned traits or other maturity steps
                              will be applied to the pawn. It is assumed that only one lifestage will use this flag.
  - **capMods** - (Optional) Capacity modifiers to be applied at this stage. Use to adjust mobility or speaking ability. Same structure as hediff capMods.
  - **backstoryIdentifier** - (Optional) The string identifier for a backstory to be applied at this stage. Will overwrite the existing backstory.
                                       Backstories may be used to apply work restrictions in lieu of another method.
  - **cannotReceiveThoughts** - (Optional) A list of ThoughtDef strings that a pawn may not acquire at this lifestage.
  - **maxWeaponMass** - (Optional) Float representation of weapon mass that this pawn is capable of using. If exceed, injury may occur when using the weapon.
  - **idleDisabled** - (Optional) Default: False. When true, this pawn will not count as an idle pawn when doing nothing.
  - **wakeOnUnhappy** - (Optional) Default: False. When true, this pawn will wake up when harmed, hungry, or other reasons. Mostly used to make babys cry obnoxiously.
  - **usesCrib** - (Optional) Default: False. When true, this pawn will prefer a crib over a normal bed.
  - **shouldBeFed** - (Optional) Default: False. When true, this pawn will be a valid target for caretaker pawns to feed.
  - **graphics** - (Optional) Defines a set of rules to apply when rendering the pawn at this lifestage.
    - **bodyType** - (Optional) A BodyTypeDef to switch to at this lifestage. More on body type switching below.
    - **clothesBodyType** - (Optional) A separate BodyTypeDef to use when resizing clothing to fit this lifestage.
    - **hairOffset** - (Optional) (x, y) Offset values to be applied to hair at this lifestage.
    - **hairScale** - (Optional) (x, y) Scale values to be applied when resizing hair at this lifestage.
    - **clothingOffsetNS** - (Optional) (x, y) Offset values to be applied to clothing when the pawn is facing north/south.
    - **clothingOffsetEW** - (Optional) (x, y) Offset values to be applied to clothing when the pawn is facing east/west.
    - **clothingScale** - (Optional) (x, y) Scale values to be applied when resizing clothing to fit this lfiestage.
    - **bodyScale** - (Optional) (x, y) Scale values to be applied to the body graphic when resizing the body for this lifestage.
    - **headScale** - (Optional) (x, y) Scale values to be applied to the head graphic when resizing the head for this lfiestage.
    - **headGraphicPath** - (Optional) Path to a graphic to be used as a head override for this lifestage.
    - **bodyGraphicsPath** - (Optional) Path to a graphic to be used as a body override for this lifestage. Not necessary if using bodyType.
    - **renderHead** - (Optional) Default: True. When false, the pawns head will not be displayed at this stage. Used when the body graphic includes the head.
    - **bodyAddons** - (Optional) A collection of rules to apply to HAR body addons
      - **bodyPart** - (Required) Maps to the same field on HAR race def. The associated part to apply the below rules to. * may be used here as a catch all.
      - **referenceBody** - (Optional) Since HAR applies offsets by bodytype and a custom bodytype may not be have offsets, this body will be used
                                       to steal the offset values from another body type. This will minimize the tweaking needed to get a good fit.
      - **renderPart** - (Optional) Default: True. When false, this bodyPart will not be displayed at this lifestage.
      - **north/south/east/west** - (Optional) Directional offset and scale values. Note that these are *additional* offset/scale values.
        - **offset** - (Required) (x, y) Offset values to be applied to this bodyPart at this lifestage.
        - **scale** - (Required) (x, y) Scale values to be applied to this bodyPart at this lifestage.
    

#### Body Types
The primary means of adjust graphics for bodies is to provide a new BodyTypeDef for each lifestage. This provides the most flexibility and reduces the probability
of complications when overriding graphics. When a pawn is generated, their original BodyTypeDef is saved to be used as their "mature" BodyTypeDef.
Once the "maturity" lifestage is reached, this body type is applied once more. 

#### Race Options
- **Pregnancy Enabled** - When true, this race may become pregnant. At the moment, humanoids will all use the same type of pregnancy. This can be adjusted with an assembly patch.
- **Lifecycle Enabled** - When true, this race will use a life cycle def to adjust lifestages per the above definition.

#### Alignment Tool
When developer logging is enabled, there is a tool in the options window. This tool may be used to tweak body addon offset/scale values
in real time in order to quickly visualize how your values will impact the rendering. The displayed value can be exported to the logs by pressing the "ex" button.
Note that these adjustments are applied to the def at runtime, so if your adjustments aren't displaying, you may be editing the wrong direction,
lifestage, or def. 

